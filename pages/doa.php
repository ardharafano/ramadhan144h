<div id="doa">
    <?php include('component/ads-banner.php'); ?>
    <div class="wrapper">
        <div class="container">
            <div class="row">

                <div class="col-lg-8">
                    <div class="bg">
                        <div class="head">
                            <a href="?page=doa-detail" target="_blank">
                                <h5>Doa Sebelum Tidur</h5>
                            </a>
                        </div>
                        <div class="head">
                            <a href="?page=doa-detail" target="_blank">
                                <h5>Doa Bangun Tidur</h5>
                            </a>
                        </div>
                        <div class="head">
                            <a href="?page=doa-detail" target="_blank">
                                <h5>Doa Ketika Mengenakan Pakaian</h5>
                            </a>
                        </div>
                        <div class="head">
                            <a href="?page=doa-detail" target="_blank">
                                <h5>Doa Ketika Mengenakan Pakaian Baru</h5>
                            </a>
                        </div>
                        <div class="head">
                            <a href="?page=doa-detail" target="_blank">
                                <h5>Doa Ketika Mengenakan Pakaian Baru 1</h5>
                            </a>
                        </div>
                        <div class="head">
                            <a href="?page=doa-detail" target="_blank">
                                <h5>Doa Ketika Mengenakan Pakaian Baru 2</h5>
                            </a>
                        </div>
                        <div class="head">
                            <a href="?page=doa-detail" target="_blank">
                                <h5>Doa Ketika Melepas atau Meletakkan Pakaian</h5>
                            </a>
                        </div>

                    </div>

                    <nav aria-label="pagination">
                        <ul class="pagination justify-content-center mt-3">
                            <li class="page-item disabled">
                                <a class="page-link">&laquo;</a>
                            </li>
                            <li class="page-item"><a class="page-link" href="#">1</a></li>
                            <li class="page-item active" aria-current="page">
                                <a class="page-link" href="#">2</a>
                            </li>
                            <li class="page-item"><a class="page-link" href="#">3</a></li>
                            <li class="page-item"><a class="page-link" href="#">4</a></li>
                            <li class="page-item"><a class="page-link" href="#">5</a></li>
                            <li class="page-item"><a class="page-link" href="#">6</a></li>
                            <li class="page-item">
                                <a class="page-link" href="#">&raquo;</a>
                            </li>
                        </ul>
                    </nav>

                </div>
                <!-- side -->

                <div class="col-lg-4">
                    <?php include('component/side-artikel.php'); ?>
                </div>

                <!-- end side -->

            </div>
        </div>
    </div>
</div>