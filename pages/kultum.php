<div id="kultum" class="mt-3 pt-lg-5 pt-0">
    <div class="wrapper">
        <div class="container">
            <div class="row">
                <div class="col-lg-8 mb-3">
                    <a href="?page=kanal">
                        <h1 class="fw-bold">Kultum</h1>
                    </a>
                    <div class="row">

                        <div class="col-lg-6 col-6">
                            <img src="assets/images/kabar/kabar-1.png" class="kultum" alt="img">
                            <tag>Kabar</tag>
                            <a href="?page=artikel">
                                <p>Lorem Ipsum is simply dummy text of the printing and typesetting industr</p>
                            </a>
                            <span>18:00 wib | Apr 24, 2023</span>

                        </div>

                        <div class="col-lg-6 col-6">
                            <img src="assets/images/kabar/kabar-1.png" class="kultum" alt="img">
                            <tag>Kabar</tag>
                            <a href="?page=artikel">
                                <p>Lorem Ipsum is simply dummy text of the printing and typesetting industr</p>
                            </a>
                            <span>18:00 wib | Apr 24, 2023</span>

                        </div>

                    </div>
                </div>

                <div class="col-lg-4">
                    <a href="?page=kanal">
                        <h1 class="fw-bold">Hijrah</h1>
                    </a>
                    <div class="row">
                        <div class="col-lg-12 mb-3">
                            <img src="assets/images/kabar/kabar-1.png" class="hijrah" alt="img">
                            <tag>Kabar</tag>
                            <a href="?page=artikel">
                                <p>Lorem Ipsum is simply dummy text of the printing and typesetting industr</p>
                            </a>
                            <span>18:00 wib | Apr 24, 2023</span>
                        </div>

                        <div class="col-lg-12">
                            <img src="assets/images/kabar/kabar-1.png" class="hijrah" alt="img">
                            <tag>Kabar</tag>
                            <a href="?page=artikel">
                                <p>Lorem Ipsum is simply dummy text of the printing and typesetting industr</p>
                            </a>
                            <span>18:00 wib | Apr 24, 2023</span>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>