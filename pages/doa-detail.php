<div id="doa">
    <?php include('component/ads-banner.php'); ?>
    <div class="wrapper">
        <div class="container">
            <div class="row">

                <div class="col-lg-8">
                    <div class="bg mb-3">
                        <div class="head text-center">
                            <a href="#">
                                <h5>Doa Sebelum Tidur</h5>
                            </a>
                            <h3>بِاسْمِكَ اللَّهُمَّ أَمُوْتُ وَأَحْيَا</h3>
                            <h5>Bismika-llaahumma amuutu wa ahyaa.</h5>
                            <h5>Artinya: "Dengan Nama-Mu ya Allah, aku mati dan aku hidup."</h5>
                        </div>
                    </div>

                    <div class="bg">
                        <div class="head">
                            <a href="?page=doa-detail" target="_blank">
                                <h5>Doa Sebelum Tidur</h5>
                            </a>
                        </div>
                        <div class="head">
                            <a href="?page=doa-detail" target="_blank">
                                <h5>Doa Bangun Tidur</h5>
                            </a>
                        </div>
                        <div class="head">
                            <a href="?page=doa-detail" target="_blank">
                                <h5>Doa Ketika Mengenakan Pakaian</h5>
                            </a>
                        </div>
                        <div class="head">
                            <a href="?page=doa-detail" target="_blank">
                                <h5>Doa Ketika Mengenakan Pakaian Baru</h5>
                            </a>
                        </div>
                        <div class="head">
                            <a href="?page=doa-detail" target="_blank">
                                <h5>Doa Ketika Mengenakan Pakaian Baru 1</h5>
                            </a>
                        </div>
                        <div class="head">
                            <a href="?page=doa-detail" target="_blank">
                                <h5>Doa Ketika Mengenakan Pakaian Baru 2</h5>
                            </a>
                        </div>
                        <div class="head">
                            <a href="?page=doa-detail" target="_blank">
                                <h5>Doa Ketika Melepas atau Meletakkan Pakaian</h5>
                            </a>
                        </div>
                    </div>

                    <div class="doa-lainnya"><a href="?page=doa">Doa Lainnya</a></div>

                </div>
                <!-- side -->

                <div class="col-lg-4">
                    <?php include('component/side-artikel.php'); ?>
                </div>

                <!-- end side -->

            </div>
        </div>
    </div>
</div>