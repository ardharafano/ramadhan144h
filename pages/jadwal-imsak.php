<div id="artikel">
    <?php include('component/ads-banner.php'); ?>
    <div class="wrapper">
        <div class="container">
            <div class="row">

                <div class="col-lg-8">

                    <img src="assets/images/jadwal-imsak/imsak.png" alt="img" class="w-100">

                    <div class="d-flex justify-content-center align-items-center align-content-center my-3">
                        <div>Share :</div>
                        <a href="#">
                            <img src="assets/images/share/fb.svg" alt="img" width="30px" height="30px" class="mx-2">
                        </a>
                        <a href="#">
                            <img src="assets/images/share/twitter.svg" alt="img" width="30px" height="30px"
                                class="mx-2">
                        </a>
                        <a href="#">
                            <img src="assets/images/share/line.svg" alt="img" width="30px" height="30px" class="mx-2">
                        </a>
                        <a href="#">
                            <img src="assets/images/share/wa.svg" alt="img" width="30px" height="30px" class="mx-2">
                        </a>
                        <a href="#">
                            <img src="assets/images/share/link.svg" alt="img" width="30px" height="30px" class="mx-2">
                        </a>
                        <a href="#">
                            <img src="assets/images/share/tele.svg" alt="img" width="30px" height="30px" class="mx-2">
                        </a>
                    </div>
                </div>

                <!-- side -->
                <div class="col-lg-4">
                    <?php include('component/side-artikel.php'); ?>
                </div>
                <!-- end side -->

            </div>
        </div>
    </div>
</div>