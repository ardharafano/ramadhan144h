<div id="ramadhan">
    <div class="wrapper">
        <div class="container">
            <div class="row">
                <div class="col-lg-4 col-sm-4">
                    <a href="?page=kalkulator-zakat">
                        <img src="assets/images/ramadhan/zakat.png" alt="img">
                    </a>
                </div>
                <div class="col-lg-4 col-sm-4">
                    <a href="?page=tadarus">
                        <img src="assets/images/ramadhan/tadarus.png" alt="img">
                    </a>
                </div>
                <div class="col-lg-4 col-sm-4">
                    <a href="?page=jadwal-imsak">
                        <img src="assets/images/ramadhan/imsak.png" alt="img">
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>