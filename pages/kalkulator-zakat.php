<div id="kalzak">
    <?php include('component/ads-banner.php'); ?>
    <div class="wrapper">
        <div class="container">
            <div class="row">

                <div class="col-lg-8">

                    <div class="tabel-zakat d-flex justify-content-between align-items-center">
                        <div class="">
                            TABEL PERHITUNGAN ZAKAT
                        </div>
                        <a href="#">
                            <div class="reset-form text-center">
                                Reset Form
                            </div>
                        </a>
                    </div>

                    <label class="bg-secondary rounded-pill w-100 text-white pt-2 pb-2 ps-4 mb-3">I.
                        Penghasilan/Pemasukan</label>

                    <div class="mb-3 row d-flex justify-content-between">
                        <label for="inputtext" class="col-sm-8 col-form-label">Pendapatan (Gaji/Perbulan)</label>
                        <div class="col-sm-4">
                            <input type="number" class="form-control" id="inputtext" value="0">
                        </div>
                    </div>

                    <div class="mb-3 row d-flex justify-content-between">
                        <label for="inputtext" class="col-sm-8 col-form-label">Pendapatan Lain-lain(/Bulan)</label>
                        <div class="col-sm-4">
                            <input type="number" class="form-control" id="inputtext" value="0">
                        </div>
                    </div>

                    <div class="mb-3 row d-flex justify-content-between">
                        <label for="inputtext" class="col-sm-8 col-form-label">Hutang/Cicilan/Pengeluaran Rutin
                            Kebutuhan Pokok (/Bulan)</label>
                        <div class="col-sm-4">
                            <input type="number" class="form-control" id="inputtext" value="0">
                        </div>
                    </div>

                    <div class="mb-3 row d-flex justify-content-between">
                        <label for="inputtext" class="col-sm-8 col-form-label text-sm-end"><b>Pemasukan/Pendapatan per
                                Bulan</b></label>
                        <div class="col-sm-4">
                            <input type="number" class="form-control" id="inputtext" value="0">
                        </div>
                    </div>

                    <label class="bg-secondary rounded-pill w-100 text-white pt-2 pb-2 ps-4 mb-3">II. Zakat Profesi
                        (Az-Zira'ah)</label>

                    <div class="mb-3 row d-flex justify-content-between">
                        <label for="inputtext" class="col-sm-8 col-form-label">Harga beras saat ini (/Kg)</label>
                        <div class="col-sm-4">
                            <input type="number" class="form-control" id="inputtext" value="0">
                        </div>
                    </div>

                    <div class="mb-3 row d-flex justify-content-between">
                        <label for="inputtext" class="col-sm-8 col-form-label">Besarnya nishab</label>
                        <div class="col-sm-4">
                            <input type="number" class="form-control" id="inputtext" value="0">
                        </div>
                    </div>

                    <div class="mb-3 row d-flex justify-content-between">
                        <label for="inputtext" class="col-sm-8 col-form-label">Wajib membayar zakat profesi?</label>
                        <div class="col-sm-4">
                            <input type="number" class="form-control" id="inputtext" value="0">
                        </div>
                    </div>

                    <div class="mb-3 row d-flex justify-content-between">
                        <label for="inputtext" class="col-sm-8 col-form-label text-sm-end"><b>Dibayarkan
                                pertahun</b></label>
                        <div class="col-sm-4">
                            <input type="number" class="form-control" id="inputtext" value="0">
                        </div>
                    </div>

                    <div class="mb-3 row d-flex justify-content-between">
                        <label for="inputtext" class="col-sm-8 col-form-label text-sm-end"><b>Dibayarkan
                                perbulan</b></label>
                        <div class="col-sm-4">
                            <input type="number" class="form-control" id="inputtext" value="0">
                        </div>
                    </div>

                    <label class="bg-secondary rounded-pill w-100 text-white pt-2 pb-2 ps-4 mb-3">III. Zakat Harta
                        Simpanan (Maal)</label>

                    <div class="mb-3 row d-flex justify-content-between">
                        <label for="inputtext" class="col-sm-8 col-form-label">Total Kekayaan (dalam bentuk tabungan /
                            investasi)</label>
                        <div class="col-sm-4">
                            <input type="number" class="form-control" id="inputtext" value="0">
                        </div>
                    </div>


                    <div class="mb-3 row d-flex justify-content-between">
                        <label for="inputtext" class="col-sm-8 col-form-label">Hutang Jatuh Tempo Saat Membayar
                            Kewajiban Zakat</label>
                        <div class="col-sm-4">
                            <input type="number" class="form-control" id="inputtext" value="0">
                        </div>
                    </div>


                    <div class="mb-3 row d-flex justify-content-between">
                        <label for="inputtext" class="col-sm-8 col-form-label">Harta Yang Wajib Di Zakat</label>
                        <div class="col-sm-4">
                            <input type="number" class="form-control" id="inputtext" value="0">
                        </div>
                    </div>


                    <div class="mb-3 row d-flex justify-content-between">
                        <label for="inputtext" class="col-sm-8 col-form-label">Besarnya nishab</label>
                        <div class="col-sm-4">
                            <input type="number" class="form-control" id="inputtext" value="0">
                        </div>
                    </div>


                    <div class="mb-3 row d-flex justify-content-between">
                        <label for="inputtext" class="col-sm-8 col-form-label">Wajib zakat maal?</label>
                        <div class="col-sm-4">
                            <input type="number" class="form-control" id="inputtext" value="0">
                        </div>
                    </div>

                    <div class="mb-3 row d-flex justify-content-between">
                        <label for="inputtext" class="col-sm-8 col-form-label text-sm-end"><b>Dibayarkan
                                pertahun</b></label>
                        <div class="col-sm-4">
                            <input type="number" class="form-control" id="inputtext" value="0">
                        </div>
                    </div>

                    <div class="mb-3 row d-flex justify-content-between">
                        <label for="inputtext" class="col-sm-8 col-form-label text-sm-end"><b>Dibayarkan perbulan
                                (Dengan cara
                                menyicil)</b></label>
                        <div class="col-sm-4">
                            <input type="number" class="form-control" id="inputtext" value="0">
                        </div>
                    </div>

                    <a href="#">
                        <div class="donasi text-center mb-3">
                            Donasi
                        </div>
                    </a>

                </div>

                <div class="col-lg-4">
                    <?php include('component/side-artikel.php'); ?>
                </div>

            </div>
        </div>
    </div>
</div>