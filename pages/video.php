<div id="video" class="mt-3 pt-lg-5">
    <div class="wrapper">

        <div class="container">
            <h1 class="fw-bold">Video</h1>
            <ul class="nav nav-pills mb-3" id="pills-tab" role="tablist">
                <li class="nav-item" role="presentation">
                    <button class="nav-link active" id="pills-takis-tab" data-bs-toggle="pill"
                        data-bs-target="#pills-takis" type="button" role="tab" aria-controls="pills-takis"
                        aria-selected="true">Takis</button>
                </li>
                <li class="nav-item" role="presentation">
                    <button class="nav-link" id="pills-potg-tab" data-bs-toggle="pill" data-bs-target="#pills-potg"
                        type="button" role="tab" aria-controls="pills-potg" aria-selected="false">POTG</button>
                </li>
                <li class="nav-item" role="presentation">
                    <button class="nav-link" id="pills-review-tab" data-bs-toggle="pill" data-bs-target="#pills-review"
                        type="button" role="tab" aria-controls="pills-review" aria-selected="false">Direveiwin</button>
                </li>
                <li class="nav-item" role="presentation">
                    <button class="nav-link" id="pills-ugc-tab" data-bs-toggle="pill" data-bs-target="#pills-ugc"
                        type="button" role="tab" aria-controls="pills-ugc" aria-selected="false">UGC</button>
                </li>
            </ul>
        </div>

        <div class="tab-content" id="pills-tabContent">
            <div class="tab-pane fade show active" id="pills-takis" role="tabpanel" aria-labelledby="pills-takis-tab">

                <div class="container mb-3">
                    <div class="row">
                        <div class="col-lg-4 content-1">
                            <div class="bg">
                                <img src="assets/images/kabar/kabar-1.png" class="head" alt="img">
                                <a href="?page=artikel">
                                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industr</p>
                                </a>
                                <span>18:00 wib | Apr 24, 2023</span>
                                <div class="centered">
                                    <a href="?page=artikel">
                                        <img src="assets/images/video/play.svg" class="play" alt="img">
                                    </a>
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-4 content-1">
                            <div class="bg">
                                <img src="assets/images/kabar/kabar-1.png" class="head" alt="img">
                                <a href="?page=artikel">
                                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industr</p>
                                </a>
                                <span>18:00 wib | Apr 24, 2023</span>
                                <div class="centered">
                                    <a href="?page=artikel">
                                        <img src="assets/images/video/play.svg" class="play" alt="img">
                                    </a>
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-4 content-1">
                            <div class="bg">
                                <img src="assets/images/kabar/kabar-1.png" class="head" alt="img">
                                <a href="?page=artikel">
                                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industr</p>
                                </a>
                                <span>18:00 wib | Apr 24, 2023</span>
                                <div class="centered">
                                    <a href="?page=artikel">
                                        <img src="assets/images/video/play.svg" class="play" alt="img">
                                    </a>
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-4 content-1">
                            <div class="bg">
                                <img src="assets/images/kabar/kabar-1.png" class="head" alt="img">
                                <a href="?page=artikel">
                                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industr</p>
                                </a>
                                <span>18:00 wib | Apr 24, 2023</span>
                                <div class="centered">
                                    <a href="?page=artikel">
                                        <img src="assets/images/video/play.svg" class="play" alt="img">
                                    </a>
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-4 content-1">
                            <div class="bg">
                                <img src="assets/images/kabar/kabar-1.png" class="head" alt="img">
                                <a href="?page=artikel">
                                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industr</p>
                                </a>
                                <span>18:00 wib | Apr 24, 2023</span>
                                <div class="centered">
                                    <a href="?page=artikel">
                                        <img src="assets/images/video/play.svg" class="play" alt="img">
                                    </a>
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-4 content-1">
                            <div class="bg">
                                <img src="assets/images/kabar/kabar-1.png" class="head" alt="img">
                                <a href="?page=artikel">
                                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industr</p>
                                </a>
                                <span>18:00 wib | Apr 24, 2023</span>
                                <div class="centered">
                                    <a href="?page=artikel">
                                        <img src="assets/images/video/play.svg" class="play" alt="img">
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <a href="#" id="loadMorecontent-1">Lebih Banyak</a>

                </div>

            </div>
            <div class="tab-pane fade" id="pills-potg" role="tabpanel" aria-labelledby="pills-potg-tab">

                <div class="container mb-3">
                    <div class="row">
                        <div class="col-lg-4 content-2">
                            <div class="bg">
                                <img src="assets/images/kabar/kabar-1.png" class="head" alt="img">
                                <a href="?page=artikel">
                                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industr</p>
                                </a>
                                <span>18:00 wib | Apr 24, 2023</span>
                                <div class="centered">
                                    <a href="?page=artikel">
                                        <img src="assets/images/video/play.svg" class="play" alt="img">
                                    </a>
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-4 content-2">
                            <div class="bg">
                                <img src="assets/images/kabar/kabar-1.png" class="head" alt="img">
                                <a href="?page=artikel">
                                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industr</p>
                                </a>
                                <span>18:00 wib | Apr 24, 2023</span>
                                <div class="centered">
                                    <a href="?page=artikel">
                                        <img src="assets/images/video/play.svg" class="play" alt="img">
                                    </a>
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-4 content-2">
                            <div class="bg">
                                <img src="assets/images/kabar/kabar-1.png" class="head" alt="img">
                                <a href="?page=artikel">
                                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industr</p>
                                </a>
                                <span>18:00 wib | Apr 24, 2023</span>
                                <div class="centered">
                                    <a href="?page=artikel">
                                        <img src="assets/images/video/play.svg" class="play" alt="img">
                                    </a>
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-4 content-2">
                            <div class="bg">
                                <img src="assets/images/kabar/kabar-1.png" class="head" alt="img">
                                <a href="?page=artikel">
                                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industr</p>
                                </a>
                                <span>18:00 wib | Apr 24, 2023</span>
                                <div class="centered">
                                    <a href="?page=artikel">
                                        <img src="assets/images/video/play.svg" class="play" alt="img">
                                    </a>
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-4 content-2">
                            <div class="bg">
                                <img src="assets/images/kabar/kabar-1.png" class="head" alt="img">
                                <a href="?page=artikel">
                                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industr</p>
                                </a>
                                <span>18:00 wib | Apr 24, 2023</span>
                                <div class="centered">
                                    <a href="?page=artikel">
                                        <img src="assets/images/video/play.svg" class="play" alt="img">
                                    </a>
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-4 content-2">
                            <div class="bg">
                                <img src="assets/images/kabar/kabar-1.png" class="head" alt="img">
                                <a href="?page=artikel">
                                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industr</p>
                                </a>
                                <span>18:00 wib | Apr 24, 2023</span>
                                <div class="centered">
                                    <a href="?page=artikel">
                                        <img src="assets/images/video/play.svg" class="play" alt="img">
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <a href="#" id="loadMorecontent-2">Lebih Banyak</a>
                </div>

            </div>
            <div class="tab-pane fade" id="pills-review" role="tabpanel" aria-labelledby="pills-review-tab">

                <div class="container mb-3">
                    <div class="row">
                        <div class="col-lg-4 content-3">
                            <div class="bg">
                                <img src="assets/images/kabar/kabar-1.png" class="head" alt="img">
                                <a href="?page=artikel">
                                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industr</p>
                                </a>
                                <span>18:00 wib | Apr 24, 2023</span>
                                <div class="centered">
                                    <a href="?page=artikel">
                                        <img src="assets/images/video/play.svg" class="play" alt="img">
                                    </a>
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-4 content-3">
                            <div class="bg">
                                <img src="assets/images/kabar/kabar-1.png" class="head" alt="img">
                                <a href="?page=artikel">
                                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industr</p>
                                </a>
                                <span>18:00 wib | Apr 24, 2023</span>
                                <div class="centered">
                                    <a href="?page=artikel">
                                        <img src="assets/images/video/play.svg" class="play" alt="img">
                                    </a>
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-4 content-3">
                            <div class="bg">
                                <img src="assets/images/kabar/kabar-1.png" class="head" alt="img">
                                <a href="?page=artikel">
                                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industr</p>
                                </a>
                                <span>18:00 wib | Apr 24, 2023</span>
                                <div class="centered">
                                    <a href="?page=artikel">
                                        <img src="assets/images/video/play.svg" class="play" alt="img">
                                    </a>
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-4 content-3">
                            <div class="bg">
                                <img src="assets/images/kabar/kabar-1.png" class="head" alt="img">
                                <a href="?page=artikel">
                                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industr</p>
                                </a>
                                <span>18:00 wib | Apr 24, 2023</span>
                                <div class="centered">
                                    <a href="?page=artikel">
                                        <img src="assets/images/video/play.svg" class="play" alt="img">
                                    </a>
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-4 content-3">
                            <div class="bg">
                                <img src="assets/images/kabar/kabar-1.png" class="head" alt="img">
                                <a href="?page=artikel">
                                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industr</p>
                                </a>
                                <span>18:00 wib | Apr 24, 2023</span>
                                <div class="centered">
                                    <a href="?page=artikel">
                                        <img src="assets/images/video/play.svg" class="play" alt="img">
                                    </a>
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-4 content-3">
                            <div class="bg">
                                <img src="assets/images/kabar/kabar-1.png" class="head" alt="img">
                                <a href="?page=artikel">
                                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industr</p>
                                </a>
                                <span>18:00 wib | Apr 24, 2023</span>
                                <div class="centered">
                                    <a href="?page=artikel">
                                        <img src="assets/images/video/play.svg" class="play" alt="img">
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <a href="#" id="loadMorecontent-3">Lebih Banyak</a>
                </div>

            </div>
            <div class="tab-pane fade" id="pills-ugc" role="tabpanel" aria-labelledby="pills-ugc-tab">

                <div class="container mb-3">
                    <div class="row">
                        <div class="col-lg-4 content-4">
                            <div class="bg">
                                <img src="assets/images/kabar/kabar-1.png" class="head" alt="img">
                                <a href="?page=artikel">
                                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industr</p>
                                </a>
                                <span>18:00 wib | Apr 24, 2023</span>
                                <div class="centered">
                                    <a href="?page=artikel">
                                        <img src="assets/images/video/play.svg" class="play" alt="img">
                                    </a>
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-4 content-4">
                            <div class="bg">
                                <img src="assets/images/kabar/kabar-1.png" class="head" alt="img">
                                <a href="?page=artikel">
                                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industr</p>
                                </a>
                                <span>18:00 wib | Apr 24, 2023</span>
                                <div class="centered">
                                    <a href="?page=artikel">
                                        <img src="assets/images/video/play.svg" class="play" alt="img">
                                    </a>
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-4 content-4">
                            <div class="bg">
                                <img src="assets/images/kabar/kabar-1.png" class="head" alt="img">
                                <a href="?page=artikel">
                                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industr</p>
                                </a>
                                <span>18:00 wib | Apr 24, 2023</span>
                                <div class="centered">
                                    <a href="?page=artikel">
                                        <img src="assets/images/video/play.svg" class="play" alt="img">
                                    </a>
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-4 content-4">
                            <div class="bg">
                                <img src="assets/images/kabar/kabar-1.png" class="head" alt="img">
                                <a href="?page=artikel">
                                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industr</p>
                                </a>
                                <span>18:00 wib | Apr 24, 2023</span>
                                <div class="centered">
                                    <a href="?page=artikel">
                                        <img src="assets/images/video/play.svg" class="play" alt="img">
                                    </a>
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-4 content-4">
                            <div class="bg">
                                <img src="assets/images/kabar/kabar-1.png" class="head" alt="img">
                                <a href="?page=artikel">
                                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industr</p>
                                </a>
                                <span>18:00 wib | Apr 24, 2023</span>
                                <div class="centered">
                                    <a href="?page=artikel">
                                        <img src="assets/images/video/play.svg" class="play" alt="img">
                                    </a>
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-4 content-4">
                            <div class="bg">
                                <img src="assets/images/kabar/kabar-1.png" class="head" alt="img">
                                <a href="?page=artikel">
                                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industr</p>
                                </a>
                                <span>18:00 wib | Apr 24, 2023</span>
                                <div class="centered">
                                    <a href="?page=artikel">
                                        <img src="assets/images/video/play.svg" class="play" alt="img">
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <a href="#" id="loadMorecontent-4">Lebih Banyak</a>
                </div>

            </div>
        </div>


    </div>
</div>

<script>
$(document).ready(function() {
    $(".content-1").slice(0, 3).show();
    $("#loadMorecontent-1").on("click", function(e) {
        e.preventDefault();
        $(".content-1:hidden").slice(0, 3).slideDown();
        if ($(".content-1:hidden").length == 0) {
            $("#loadMorecontent-1").text("Tidak ada video lagi").addClass("noContent");
        }
    });

})

$(document).ready(function() {
    $(".content-2").slice(0, 3).show();
    $("#loadMorecontent-2").on("click", function(e) {
        e.preventDefault();
        $(".content-2:hidden").slice(0, 3).slideDown();
        if ($(".content-2:hidden").length == 0) {
            $("#loadMorecontent-2").text("Tidak ada video lagi").addClass("noContent");
        }
    });

})

$(document).ready(function() {
    $(".content-3").slice(0, 3).show();
    $("#loadMorecontent-3").on("click", function(e) {
        e.preventDefault();
        $(".content-3:hidden").slice(0, 3).slideDown();
        if ($(".content-3:hidden").length == 0) {
            $("#loadMorecontent-3").text("Tidak ada video lagi").addClass("noContent");
        }
    });

})

$(document).ready(function() {
    $(".content-4").slice(0, 3).show();
    $("#loadMorecontent-4").on("click", function(e) {
        e.preventDefault();
        $(".content-4:hidden").slice(0, 3).slideDown();
        if ($(".content-4:hidden").length == 0) {
            $("#loadMorecontent-4").text("Tidak ada video lagi").addClass("noContent");
        }
    });

})
</script>