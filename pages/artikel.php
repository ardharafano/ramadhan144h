<div id="artikel">
    <?php include('component/ads-banner.php'); ?>
    <div class="wrapper">
        <div class="container">
            <div class="row">

                <div class="col-lg-8">
                    <h4>NEWS</h4>
                    <h3>HARI PANGAN SEDUNIA 2022, Mentri pertanian Syahrul Yasin Limpo ajak semua pihak berkolaborasi
                        untuk kecukupan pangan dunia.</h3>
                    <p class="date">Senin, 12 Desember 2022</p>
                    <img src="assets/images/kabar/kabar-1.png" alt="img" class="isi" />
                    <figcaption>Direktur Eksekutif Charta Politika Yunarto Wijaya. [Suara.com/Fakhri Fuadi]</figcaption>

                    <ol>
                        <li>
                            Gambar Ucapan Idul Fitri 2023
                            <a href="#">https://www.freepik.com/free-vector/hand-drawn-eid-alfitr-illustration-background_25394531.htm#query=idul%20fitri%202023&position=3&from_view=search&track=ais</a>
                        </li>
                        <li>
                            Gambar Ucapan Idul Fitri 2023
                            <a href="#">https://www.freepik.com/free-vector/hand-drawn-eid-alfitr-illustration-background_25394531.htm#query=idul%20fitri%202023&position=3&from_view=search&track=ais</a>
                        </li>
                        <li>
                            Gambar Ucapan Idul Fitri 2023
                            <a href="#">https://www.freepik.com/free-vector/hand-drawn-eid-alfitr-illustration-background_25394531.htm#query=idul%20fitri%202023&position=3&from_view=search&track=ais</a>
                        </li>
                        <li>
                            Gambar Ucapan Idul Fitri 2023
                            <a href="#">https://www.freepik.com/free-vector/hand-drawn-eid-alfitr-illustration-background_25394531.htm#query=idul%20fitri%202023&position=3&from_view=search&track=ais</a>
                        </li>
                        <li>
                            Gambar Ucapan Idul Fitri 2023
                            <a href="#">https://www.freepik.com/free-vector/hand-drawn-eid-alfitr-illustration-background_25394531.htm#query=idul%20fitri%202023&position=3&from_view=search&track=ais</a>
                        </li>
                    </ol>

                    <p class="isi">JAKARTA - Otorita Ibu Kota Nusantara (IKN) mengharapkan seluruh pemangku kepentingan
                        memahami substansi pengaturan dalam Undang-Undang Nomor 3 Tahun 2022 tentang Ibu Kota Negara (UU
                        IKN) dan peraturan pelaksanaannya. Hal tersebut disampaikan
                        Sekretaris Otorita Ibu Kota Nusantara (IKN) Achmad Jaka Santos Adiwijaya saat menghadiri
                        Sosialisasi Undang-Undang Nomor 3 Tahun 2022 tentang Ibu Kota Negara (UU IKN) dan Peraturan
                        Pelaksanaan UU IKN yang diselenggarakan pada 19-20
                        Oktober 2022 oleh Kementerian PPN/Bappenas.</p>
                    <p class="isi">“Sosialisasi yang dilakukan saat ini menjadi salah satu faktor kunci karena dengan
                        semakin meluasnya pemahaman terhadap substansi UU 3/2022 dan peraturan pelaksanaannya, akan
                        meningkatkan kualitas kerjasama para stakeholders untuk
                        membangun Ibu Kota Nusantara,” kata Jaka pada Rabu (19/10/2022), di Jakarta Pusat. </p>
                    <p class="isi">Ia meyakini bahwa ke depannya IKN akan memiliki posisi strategis sebagai pusat
                        pertumbuhan regional dan nasional. “Pembangunan ibu kota negara tidak berdiri sendiri. Rencana
                        terpadu ekosistem tiga kota akan dikembangkan dengan poros
                        IKN, Samarinda, Balikpapan, dan dengan daerah mitra sekitarnya,” kata Jaka.</p>
                    <p class="isi">Dalam kesempatan yang sama, Plt. Staf Ahli Bidang Hubungan Kelembagaan, Kementerian
                        PPN/Bappenas Oktorialdi mengharapkan para peserta dapat menyampaikan masukannya secara
                        konstruktif. Hal ini penting karena forum ini selain memberikan
                        pemahaman juga sebagai ruang partisipasi untuk penyusunan peraturan perundang-undangan dan
                        kebijakan selanjutnya. </p>
                    <p class="isi">Terdapat empat peraturan yang dipaparkan dalam forum sosialisasi hari pertama ini,
                        yakni UU IKN, Peraturan Pemerintah (PP) No. 17 Tahun 2022 tentang Pendanaan dan Pengelolaan
                        Anggaran dalam Rangka Persiapan, Pembangunan dan Pemindahan
                        IKN; Peraturan Presiden (Perpres) No. 65 Tahun 2022 tentang Perolehan Tanah dan Pengelolaan
                        Pertanahan di IKN; dan Perpres No. 64 Tahun 2022 tentang Rencana Tata Ruang Kawasan Strategis
                        Nasional IKN.</p>
                    <p class="isi">Setelah diundangkannya UU IKN dan sejumlah peraturan pelaksanaannya, amanah dari UU
                        No. 12 Tahun 2011 tentang Pembentukan Peraturan Perundang-undangan juncto UU No. 15 Tahun 2019
                        dan UU No. 13 Tahun 2022 telah mengatur mengenai kewajiban
                        sosialisasi dari peraturan perundangan kepada masyarakat. Oleh karena itu acara ini
                        diselenggarakan dengan tujuan untuk penyebarluasan, pemantauan, dan sosialisasi peraturan
                        perundangan kepada masyarakat.</p>
                    <p class="isi">Selain itu, sebagai upaya mewujudkan penguatan keterlibatan dan partisipasi kepada
                        masyarakat yang bermakna (meaningful participation) konsultasi publik dilakukan untuk memenuhi
                        tiga prasyarat, yaitu hak untuk didengarkan pendapatnya
                        (right to be heard), hak untuk dipertimbangkan pendapatnya (right to be considered), dan hak
                        untuk mendapatkan penjelasan atau jawaban atas pendapat yang diberikan (right to be explained).
                    </p>
                    <p class="isi">Hadir dalam pertemuan ini para pemangku kepentingan dari berbagai lapisan seperti
                        kementerian/lembaga, pemerintah daerah, paguyuban, lembaga adat, forum agama, organisasi
                        masyarakat Kalimantan Timur, perguruan tinggi, hingga media
                        massa.
                    </p>

                    <div class="tag">
                        <a href="#">
                            <p>IKN</p>
                        </a>

                        <a href="#">
                            <p>NUSANTARA</p>
                        </a>

                        <a href="#">
                            <p>OTORITA IKN</p>
                        </a>

                        <a href="#">
                            <p>BAMBANG SUSANTONO</p>
                        </a>

                        <a href="#">
                            <p>DHONY RAHAJOE</p>
                        </a>

                        <a href="#">
                            <p>INVESTASI IKN</p>
                        </a>

                        <a href="#">
                            <p>PEMBANGUNAN IKN</p>
                        </a>

                    </div>

                    <div class="d-flex justify-content-center align-items-center align-content-center my-3">
                        <div>Share :</div>
                        <a href="#">
                            <img src="assets/images/share/fb.svg" alt="img" width="30px" height="30px" class="mx-2">
                        </a>
                        <a href="#">
                            <img src="assets/images/share/twitter.svg" alt="img" width="30px" height="30px"
                                class="mx-2">
                        </a>
                        <a href="#">
                            <img src="assets/images/share/line.svg" alt="img" width="30px" height="30px" class="mx-2">
                        </a>
                        <a href="#">
                            <img src="assets/images/share/wa.svg" alt="img" width="30px" height="30px" class="mx-2">
                        </a>
                        <a href="#">
                            <img src="assets/images/share/link.svg" alt="img" width="30px" height="30px" class="mx-2">
                        </a>
                        <a href="#">
                            <img src="assets/images/share/tele.svg" alt="img" width="30px" height="30px" class="mx-2">
                        </a>
                    </div>
                </div>

                <!-- side -->

                <div class="col-lg-4">
                    <?php include('component/side-artikel.php'); ?>
                </div>

                <!-- end side -->

            </div>
        </div>
    </div>
</div>