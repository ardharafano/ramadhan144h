<div id="tadarus">
    <?php include('component/ads-banner.php'); ?>

    <div class="tadarus">
        <div class="wrapper">
            <div class="container">
                <div class="row">

                    <div class="col-lg-8">
                        <div class="wrap-tadarus">
                            <div class="header-tadarus">
                                <h1>
                                    Tadarus Al-Qur'an
                                </h1>
                                <div class="pilihan">
                                    <div class="form-group-select">
                                        <select class="form-control">
                                            <option>1. Al Fatihah</option>
                                            <option>2. Al Baqarah</option>
                                        </select>
                                    </div>
                                    <div class="form-group-select">
                                        <select class="form-control">
                                            <option>Ayat 1-5</option>
                                            <option>Ayat 1-7</option>
                                        </select>
                                    </div>
                                    <div class="form-group-select">
                                        <select class="form-control">
                                            <option>Bookmark</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="body-tadarus">
                                <ul class="ayat">
                                    <li class="list-ayat">
                                        <p class="arab">
                                            بِسْمِ اللّٰهِ الرَّحْمٰنِ الرَّحِيْمِ
                                        </p>
                                        <p class="terjemahan">
                                            1. Dengan nama Allah Yang Maha Pengasih, Maha Penyayang.
                                        </p>
                                        <div class="more-info">
                                            <a href="javascripts:;" class="voice">
                                                <img src="assets/images/tadarus/speaker.svg" alt="img" width="24"
                                                    height="24">
                                            </a>
                                            <a href="javascripts:;" class="bookmark">
                                                <img src="assets/images/tadarus/bookmark.svg" alt="img" width="24"
                                                    height="24">
                                            </a>
                                        </div>
                                    </li>
                                    <li class="list-ayat">
                                        <p class="arab">
                                            اَلْحَمْدُ لِلّٰهِ رَبِّ الْعٰلَمِيْنَۙ
                                        </p>
                                        <p class="terjemahan">
                                            2. Segala puji bagi Allah, Tuhan seluruh alam,
                                        </p>
                                        <div class="more-info">
                                            <a href="javascripts:;" class="voice">
                                                <img src="assets/images/tadarus/speaker.svg" alt="img" width="24"
                                                    height="24">
                                            </a>
                                            <a href="javascripts:;" class="bookmark">
                                                <img src="assets/images/tadarus/bookmark.svg" alt="img" width="24"
                                                    height="24">
                                            </a>
                                        </div>
                                    </li>
                                    <li class="list-ayat">
                                        <p class="arab">
                                            الرَّحْمٰنِ الرَّحِيْمِۙ
                                        </p>
                                        <p class="terjemahan">
                                            3. Yang Maha Pengasih, Maha Penyayang,
                                        </p>
                                        <div class="more-info">
                                            <a href="javascripts:;" class="voice">
                                                <img src="assets/images/tadarus/speaker.svg" alt="img" width="24"
                                                    height="24">
                                            </a>
                                            <a href="javascripts:;" class="bookmark">
                                                <img src="assets/images/tadarus/bookmark.svg" alt="img" width="24"
                                                    height="24">
                                            </a>
                                        </div>
                                    </li>
                                    <li class="list-ayat">
                                        <p class="arab">
                                            مٰلِكِ يَوْمِ الدِّيْنِۗ
                                        </p>
                                        <p class="terjemahan">
                                            4. Pemilik hari pembalasan.
                                        </p>
                                        <div class="more-info">
                                            <a href="javascripts:;" class="voice">
                                                <img src="assets/images/tadarus/speaker.svg" alt="img" width="24"
                                                    height="24">
                                            </a>
                                            <a href="javascripts:;" class="bookmark">
                                                <img src="assets/images/tadarus/bookmark.svg" alt="img" width="24"
                                                    height="24">
                                            </a>
                                        </div>
                                    </li>
                                    <li class="list-ayat">
                                        <p class="arab">
                                            اِيَّاكَ نَعْبُدُ وَاِيَّاكَ نَسْتَعِيْنُۗ
                                        </p>
                                        <p class="terjemahan">
                                            5. Hanya kepada Engkaulah kami menyembah dan hanya kepada Engkaulah kami
                                            mohon
                                            pertolongan.
                                        </p>
                                        <div class="more-info">
                                            <a href="javascripts:;" class="voice">
                                                <img src="assets/images/tadarus/speaker.svg" alt="img" width="24"
                                                    height="24">
                                            </a>
                                            <a href="javascripts:;" class="bookmark">
                                                <img src="assets/images/tadarus/bookmark.svg" alt="img" width="24"
                                                    height="24">
                                            </a>
                                        </div>
                                    </li>
                                    <li class="list-ayat">
                                        <p class="arab">
                                            اِهْدِنَا الصِّرَاطَ الْمُسْتَقِيْمَ ۙ
                                        </p>
                                        <p class="terjemahan">
                                            6. Tunjukilah kami jalan yang lurus,
                                        </p>
                                        <div class="more-info">
                                            <a href="javascripts:;" class="voice">
                                                <img src="assets/images/tadarus/speaker.svg" alt="img" width="24"
                                                    height="24">
                                            </a>
                                            <a href="javascripts:;" class="bookmark">
                                                <img src="assets/images/tadarus/bookmark.svg" alt="img" width="24"
                                                    height="24">
                                            </a>
                                        </div>
                                    </li>
                                    <li class="list-ayat">
                                        <p class="arab">
                                            صِرَاطَ الَّذِيْنَ اَنْعَمْتَ عَلَيْهِمْ ەۙ غَيْرِ الْمَغْضُوْبِ عَلَيْهِمْ
                                            وَلَا
                                            الضَّاۤلِّيْنَ
                                        </p>
                                        <p class="terjemahan">
                                            7. (yaitu) jalan orang-orang yang telah Engkau beri nikmat kepadanya; bukan
                                            (jalan)
                                            mereka yang
                                            dimurkai, dan bukan (pula jalan) mereka yang sesat.
                                        </p>
                                        <div class="more-info">
                                            <a href="javascripts:;" class="voice">
                                                <img src="assets/images/tadarus/speaker.svg" alt="img" width="24"
                                                    height="24">
                                            </a>
                                            <a href="javascripts:;" class="bookmark">
                                                <img src="assets/images/tadarus/bookmark.svg" alt="img" width="24"
                                                    height="24">
                                            </a>
                                        </div>
                                    </li>
                                </ul>
                                <div class="next-ayat">
                                    <a href="#">
                                        < AYAT 1-6 </a>
                                            <a href="#">
                                                AYAT 6-7 >
                                            </a>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-4">
                        <?php include('component/side-artikel.php'); ?>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>