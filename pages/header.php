<div id="header">
    <div class="container">
        <div class="wrapper">
            <div class="row">
                <div class="col-lg-4">
                    <img src="assets/images/header/header-1.svg" class="header-1" alt="img">
                    <div class="position-relative">
                        <img src="assets/images/header/count-down.svg" class="count-down" alt="img">
                        <div class="centered">
                        <h5>Menuju Waktu Berbuka</h5>
                        <p>05:35:45</p>
                        <h5>Jakarta & Sekitarnya</h5>
                        </div>
                    </div>

                </div>

                <div class="col-lg-8">
                    <img src="assets/images/header/header-2.svg" class="header-2" alt="img">
                </div>
            </div>
        </div>
    </div>
</div>