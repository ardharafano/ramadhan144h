<div id="sosial">
    <div class="wrapper">
        <div class="container">
            <h1 class="fw-bold">Ikuti Kami</h1>
            <div class="sosmed d-flex justify-content-center align-items-center">
                <a href="#">
                    <img src="assets/images/footer/fb.svg" class="kultum" alt="img">
                </a>

                <a href="#">
                    <img src="assets/images/footer/twt.svg" class="kultum" alt="img">
                </a>

                <a href="#">
                    <img src="assets/images/footer/yt.svg" class="kultum" alt="img">
                </a>

                <a href="#">
                    <img src="assets/images/footer/ig.svg" class="kultum" alt="img">
                </a>
            </div>
            <p>Dapatkan informasi terkini dan terbaru yang dikirimkan langsung ke Inbox anda</p>
        </div>
    </div>
</div>

<div id="redaksi">
    <div class="wrapper">
        <div class="container">
            <div class="foot d-flex flex-wrap gap-3 justify-content-center align-items-center">
                <a href="#">
                    Redaksi
                </a>
                <a href="#">
                    Kontak
                </a>
                <a href="#">
                    Tentang Kami
                </a>
                <a href="#">
                    Karir
                </a>
                <a href="#">
                    Pedoman Media Siber
                </a>
                <a href="#">
                    Site Map
                </a>
            </div>
        </div>
    </div>
</div>

<div id="copyright">
    <div class="wrapper">
        <div class="container">
            <p class="text-center">© 2023 suara.com - All Rights Reserved.</p>
        </div>
    </div>
</div>