<div id="nav">
    <nav class="navbar navbar-expand-xl fixed-top navbar-light bg-light" aria-label="Eighth navbar example">
        <div class="container">
            <a class="navbar-brand d-xl-none fw-bold" href="index.php">Suara Ramadhan 1444h</a>
            <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarsExample07"
                aria-controls="navbarsExample07" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse justify-content-md-center" id="navbarsExample07">
                <ul class="navbar-nav mb-2 mb-lg-0">
                    <li class="nav-item me-1">
                        <a class="nav-link d-none d-sm-block fw-bold" aria-current="page" href="index.php"><img
                                src="assets/images/home.svg" width="20" height="20"></a>
                    </li>
                    <li class="nav-item me-1">
                        <a class="nav-link fw-bold" aria-current="page" href="index.php#kabar">Kabar</a>
                    </li>
                    <li class="nav-item me-1">
                        <a class="nav-link fw-bold" aria-current="page" href="index.php#selebritis">Selebritis</a>
                    </li>
                    <li class="nav-item me-1">
                        <a class="nav-link fw-bold" aria-current="page" href="index.php#serbaserbi">SerbaSerbi</a>
                    </li>
                    <li class="nav-item me-1">
                        <a class="nav-link fw-bold" aria-current="page" href="index.php#kultum">Kultum</a>
                    </li>
                    <li class="nav-item me-1">
                        <a class="nav-link fw-bold" aria-current="page" href="index.php#kultum">Hijrah</a>
                    </li>
                    <li class="nav-item me-1">
                        <a class="nav-link fw-bold" aria-current="page" href="?page=kalkulator-zakat">Kalkulator
                            Zakat</a>
                    </li>
                    <li class="nav-item me-1">
                        <a class="nav-link fw-bold" aria-current="page" href="?page=tadarus">Tadarus Al-Quran</a>
                    </li>
                    <li class="nav-item me-1">
                        <a class="nav-link fw-bold" aria-current="page" href="?page=jadwal-imsak">Jadwal Imsakiyah</a>
                    </li>
                    <li class="nav-item me-1">
                        <a class="nav-link fw-bold" aria-current="page" href="index.php#video">Video</a>
                    </li>
                    <li class="nav-item me-1">
                        <div class="search-container">
                            <div class="search-wrap" id="search-button">
                                <img src="assets/images/search.svg" width="30" height="30">
                            </div>
                        </div>

                        <div>
                            <form action="#">
                                <div class="wrap-search-form" id="search-form" style="display: none;">
                                    <input class="input-search" placeholder="Cari Disini">
                                    <button class="button-search">
                                        <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16"
                                            fill="currentColor" class="bi bi-search" viewBox="0 0 16 16">
                                            <path
                                                d="M11.742 10.344a6.5 6.5 0 1 0-1.397 1.398h-.001c.03.04.062.078.098.115l3.85 3.85a1 1 0 0 0 1.415-1.414l-3.85-3.85a1.007 1.007 0 0 0-.115-.1zM12 6.5a5.5 5.5 0 1 1-11 0 5.5 5.5 0 0 1 11 0z" />
                                        </svg>
                                    </button>
                                </div>
                            </form>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
    </nav>
</div>

<!-- start search button -->
<script>
$("#search-button").click(function() {
    $("#search-form").slideToggle();
});
</script>
<!-- end search button -->