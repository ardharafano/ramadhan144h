<!DOCTYPE html>
<html lang="en">

<head>
    <title>Suara Ramadhan 1444H</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="Suara Ramadhan 1444H">
    <meta name="keywords" content="Suara, Suara Ramadhan 1444H, Tanpa Suara Beda Artinya">

    <meta property="og:title" content="suara.com" />
    <meta property="og:description" content="Suara Ramadhan 144H" />
    <meta property="og:url" content="https://www.suara.com/" />
    <meta property="og:image" content="assets/images/icon_suara.png" />

    <link rel="Shortcut icon" href="assets/images/icon_suara.png">
    <link rel="stylesheet" href="assets/css/main.css?<?= time() ?>" />
    <!-- <link rel="stylesheet" href="assets/css/splide.min.css"> -->
    <link href="assets/css/bootstrap.min.css?<?= time() ?>" rel="stylesheet">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link
        href="https://fonts.googleapis.com/css2?family=Montserrat:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&display=swap"
        rel="stylesheet">

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
    <!-- <script src="https://cdn.jsdelivr.net/npm/@splidejs/splide@3/dist/js/splide.min.js"></script> -->
    <script src="assets/js/bootstrap.bundle.min.js?<?= time() ?>"></script>
</head>

<body>
    <?php include('component/navbar.php'); ?>
    <?php include('component/ads-side.php'); ?>
    <?php
            if(isset($_GET['page'])){ 
                $url = 'index.php?';
                include("pages/".$_GET['page'].".php");
            }else{
                $url = 'index.php?';
                include("pages/home.php");
            }
        ?>
    <?php include('component/footer.php'); ?>

</body>

<!-- <script>
$(document).ready(function() {
    jQuery('img').each(function() {
        jQuery(this).attr('src', jQuery(this).attr('src') + '?' + (new Date()).getTime());
    });
});
</script> -->

</html>